using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Stats : MonoBehaviour
{
    public float statFood;
    public float statOxygen;
    public float statWater;

    public float timeToDecreaseFood;
    public float timeToDecreaseOxygen;
    public float timeToDecreaseWater;

    public float amountToSubtractFood;
    public float amountToSubtractOxygen;
    public float amountToSubtractWater;

    [HideInInspector]
    public int amountOfFood = 0;
    [HideInInspector]
    public int amountOfOxygen = 0;
    [HideInInspector]
    public int amountOfWater = 0;

    private void FixedUpdate()
    {
        //All the time the stats decrese
        statFood -= amountToSubtractFood * timeToDecreaseFood * Time.deltaTime;
        statOxygen -= amountToSubtractOxygen * timeToDecreaseOxygen * Time.deltaTime;
        statWater -= amountToSubtractWater * timeToDecreaseWater * Time.deltaTime;

        if (statFood <= 0 || statOxygen <= 0 || statWater <= 0) //When some stat its equal 0 the game ends
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
