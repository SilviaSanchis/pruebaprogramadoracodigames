using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AstronautMovement : MonoBehaviour, IDragHandler, IEndDragHandler
{   
    public float speed = 80f;
    public float turnSpeed = 10f;
    public LayerMask groundCheck;
    private Rigidbody rB;

    private GameObject[] waypoints;
    private GameObject[] crates;
    private GameObject closestBox = null;
    [HideInInspector]
    public bool crateTouch = false;

    private int randomNum = 0;
    private float comparerDis;
    public float distanceMin;

    private bool wayP = false;
    private Vector3 directionToPoint;
    private Quaternion rotation;

    private bool draging = false;
    private RaycastHit hit;

    private UiManager uiManager;
    private Camera camUi;

    void Start()
    {
        camUi = GetComponentInChildren<Camera>();
        rB = gameObject.GetComponent<Rigidbody>();
        waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
        uiManager = FindObjectOfType<UiManager>();
    }

    void FixedUpdate()
    {
        crates = GameObject.FindGameObjectsWithTag("Crate");
        if (closestBox != null && draging == false)
        {
            if (closestBox.CompareTag("Crate"))//Go to the near crate while this crate is not open
            {
                directionToPoint = closestBox.transform.position - transform.position;
                rotation = Quaternion.LookRotation(directionToPoint);

                transform.position = Vector3.MoveTowards(transform.position, closestBox.transform.position, speed * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, rotation, turnSpeed * Time.deltaTime);
            }
            else
            {
                closestBox = null;
            }
        }
        else if(draging == false && closestBox == null)
        {
            ToNearCrate();
            //if the astronaut don't detect any crate on the floor then go to waypoints
            if (wayP == false)
            {
                directionToPoint = waypoints[randomNum].transform.position - transform.position;
                rotation = Quaternion.LookRotation(directionToPoint);

                transform.position = Vector3.MoveTowards(transform.position, waypoints[randomNum].transform.position, speed * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, rotation, turnSpeed * Time.deltaTime);
            }
            else
            {
                wayP = false;
            }
        }
    }

    //Find the nearest crate in range at the scene
    private GameObject ToNearCrate()
    {
        for (int i = 0; i < crates.Length; i++)
        {
            comparerDis = Vector3.Distance(transform.position, crates[i].transform.position);
            if (comparerDis <= distanceMin)
            {
                closestBox = crates[i];
            }
        }
        crateTouch = false;
        return closestBox;        
    }

    void OnTriggerEnter(Collider col)
    {   //When the astronaut arrives to one waypoint then he will find other
        if (col.gameObject.CompareTag("Waypoint"))
        {
            wayP = true;
            randomNum = Random.Range(1, waypoints.Length);
            if (col.gameObject == waypoints[randomNum])
            {
                randomNum = Random.Range(1, waypoints.Length);                
            }
        }

        //When the astronaut open the crate, this is deleted in the pull of crates
        if (col.gameObject.CompareTag("Crate"))
        {
            crateTouch = true;
            ToNearCrate();
            closestBox = null;
            for(int i = 0; i < crates.Length; i++)
            {
                if (col.gameObject == crates[i])
                {
                    crates[i] = null;
                }
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Waypoint"))
        {
            wayP = false;
        }
    }

    //These functions detect the mouse and drags the astronaut with the mouse
    public void OnDrag(PointerEventData eventData)
    {
        draging = true;
        rB.isKinematic = true;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 10000, groundCheck))
        {
            transform.position = hit.point;
        }        
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        draging = false;
        rB.isKinematic = false;
    }

    //Open the inventory
    private void OnMouseUp()
    {
        uiManager.touchedAstronaut = gameObject;
        uiManager.cam = camUi;
        uiManager.stats = GetComponent<Stats>();
    }
}
