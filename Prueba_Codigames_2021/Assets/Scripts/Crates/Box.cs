using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    private bool opened = false;
    private float timer = 5f;

    public int maxAmountOfItems;
    private int amountOfType = 0;
    private int typeOfItem = 0;
    private Stats stats;

    private void FixedUpdate()
    {
        if (opened == true)
        {
            timer -= 1f * Time.deltaTime;
            if (timer <= 0f)
            {
                Destroy(gameObject);
            }
        }
    }

    //if the crate touch the terrain change his tag to make it targetable
    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Floor") && opened == false)
        {
            gameObject.tag = "Crate";
        }
    }

    //if the crate was touched one time the other astrounatus can't detect it
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && gameObject.CompareTag("Crate"))
        {
            gameObject.tag = "Untagged";
            opened = true;

            Reward(other.gameObject);
        }
    }

   void Reward(GameObject astronaut)//This function make a random reward in al crates but we can change the amount of reward with the variable maxAmountOfItems
    {
        stats= astronaut.GetComponent<Stats>();
        amountOfType = Random.Range(1,maxAmountOfItems);
        for (int i = 0; i < amountOfType; i++)
        {
            typeOfItem = Random.Range(0, 3);
            if (typeOfItem == 0)
            {
                stats.amountOfFood++;
            }
            else if (typeOfItem == 1)
            {
                stats.amountOfOxygen++;
            }
            else if (typeOfItem == 2)
            {
                stats.amountOfWater++;
            }
        }       
    }
}
