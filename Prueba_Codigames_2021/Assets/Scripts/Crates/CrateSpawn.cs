using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrateSpawn : MonoBehaviour
{
    public Vector3 areaSize;
    public GameObject crates;
    public float timeToSpawn = 5f;

    private float timer;

    void Start()
    {
        SpawnCrate();
    }

    void FixedUpdate()
    {
        timer -= 1f * Time.deltaTime;//The time to spawns another crate
        if (timer <= 0f)
        {
            SpawnCrate();
        }
    }

    void SpawnCrate()//Spwans a crate at random position in the area
    {
        Vector3 pos = transform.position + new Vector3(Random.Range(-areaSize.x / 2, areaSize.x / 2), Random.Range(-areaSize.y / 2, areaSize.y / 2), Random.Range(-areaSize.z / 2, areaSize.z / 2));

        Instantiate(crates, pos, Quaternion.identity, gameObject.transform);
        timer = timeToSpawn;
    }

    private void OnDrawGizmosSelected()//Whit this function we visualize the area to make more easy the changes 
    {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(transform.position, areaSize);
    }
}
