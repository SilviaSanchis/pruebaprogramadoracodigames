using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public GameObject itemBase;
    public GameObject content;

    private UiManager uiManager;
    private Stats stats;

    private int totalItems = 0;
    private int totalItemsFood = 0;
    private int totalItemsOxygen = 0;
    private int totalItemsWater = 0;
    private int contadorFood = 0;
    private int contadorOxygen = 0;
    private int contadorWater = 0;

    private GameObject[] items;
    public GameObject selectedAstronaut;

    [HideInInspector]
    public int statType;

    private void Start()
    {
        uiManager = FindObjectOfType<UiManager>();
        selectedAstronaut = null;
    }

    void Update()
    {
        selectedAstronaut = uiManager.touchedAstronaut;

        if (selectedAstronaut != null)//When some astronaut was selected shows his items in the UI, if they recolect more items then this recharge the inventory
        {
            items = GameObject.FindGameObjectsWithTag("Atribute");
            stats = selectedAstronaut.GetComponent<Stats>();

            totalItems = 0;
            totalItemsFood = 0;
            totalItemsOxygen = 0;
            totalItemsWater = 0;
            contadorFood = 0;
            contadorOxygen = 0;
            contadorWater = 0;


            totalItemsFood = stats.amountOfFood;
            totalItemsOxygen = stats.amountOfOxygen;
            totalItemsWater = stats.amountOfWater;
            contadorFood = totalItemsFood;
            contadorOxygen = totalItemsOxygen;
            contadorWater = totalItemsWater;

            totalItems = totalItemsFood + totalItemsOxygen + totalItemsWater;

            if (totalItems != 0 && items.Length != totalItems)
            {
                DestroyItems();
                for (int i = 0; i < totalItems; i++)
                {
                    DetermineType();
                }
            }
            else if (totalItems == 0)
            {
                DestroyItems(); 
            }
        }
        else//When close the inventory delete and make 0 all variables 
        {
            stats = null;
            totalItems = 0;
            totalItemsFood = 0;
            totalItemsOxygen = 0;
            totalItemsWater = 0;
            contadorFood = 0;
            contadorOxygen = 0;
            contadorWater = 0;
            DestroyItems();
        }
    }

    void DestroyItems()//This clean the array of items and items in scene
    {
        items = GameObject.FindGameObjectsWithTag("Atribute");
        for (int a = 0; a < items.Length; a++)
        {
            DestroyImmediate(items[a]);
        }
        items = null;
    }

    void DetermineType()//When the program instantiate the items in the inventory first this determine the type of item
    {
        if (totalItemsFood != 0 && contadorFood > 0)
        {
            statType = 0;
            --contadorFood;
        }
        else if (totalItemsOxygen !=0 && contadorOxygen > 0)
        {
            statType = 1;
            --contadorOxygen;
        }
        else if (totalItemsWater != 0 && contadorWater > 0)
        {
            statType = 2;
            --contadorWater;
        }

        Instantiate(itemBase, content.transform);
    }

}
