using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AtrubuteDisplay : MonoBehaviour
{
    public ScriptableManager food;
    public ScriptableManager oxygen;
    public ScriptableManager water;

    private Text nameText;
    private Image artworkItem;

    [HideInInspector]
    public int atrubuteToShow;

    private Inventory inventory;
    private Stats stats;

    private void Awake()
    {
        inventory = FindObjectOfType<Inventory>();
        stats = inventory.selectedAstronaut.GetComponent<Stats>();
        nameText = GetComponentInChildren<Text>();
        artworkItem = transform.GetChild(0).gameObject.GetComponent<Image>();
        atrubuteToShow = inventory.statType;
        DisplayAtribute();
    }

    void DisplayAtribute()//Set the type of the atribute to show the correct item
    {
        if (atrubuteToShow == 0)
        {
            nameText.text = food.name;
            artworkItem.sprite = food.artwork;
        }
        else if (atrubuteToShow == 1)
        {
            nameText.text = oxygen.name;
            artworkItem.sprite = oxygen.artwork;
        }
        else if (atrubuteToShow == 2)
        {
            nameText.text = water.name;
            artworkItem.sprite = water.artwork;
        }
    }


    public void Use()//When the item is selected in the inventory, the recovery number is added to the corresponding type
    {
        if (atrubuteToShow == 0)
        {
            --stats.amountOfFood;
            stats.statFood += food.recoveryNum;
            if (stats.statFood > 1)
            {
                stats.statFood = 1;
            }
        }
        else if (atrubuteToShow == 1)
        {
            --stats.amountOfOxygen;
            stats.statOxygen += oxygen.recoveryNum;
            if (stats.statOxygen > 1)
            {
                stats.statOxygen = 1;
            }
        }
        else if (atrubuteToShow == 2)
        {
            --stats.amountOfWater;
            stats.statWater += water.recoveryNum;
            if (stats.statWater > 1)
            {
                stats.statWater = 1;
            }
        }
    }
}
