using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    [HideInInspector]
    public GameObject touchedAstronaut;
    [HideInInspector]
    public Camera cam;
    [HideInInspector]
    public Stats stats;
    [HideInInspector]
    public bool clickSpawn = false;
    [HideInInspector]
    public bool closeButton = false;

    public GameObject panelInventory;
    public InputField textName;
    public GameObject buttonSpawn;

    public Image progresBarFood;
    public Image progresBarOxygen;
    public Image progresBarWater;

    public Image progresBarVSFood;
    public Image progresBarVSOxygen;
    public Image progresBarVSWater;

    private GameObject[] astronauts;
    private float sumOfFood = 0;
    private float sumOfOxygen = 0;
    private float sumOfWater = 0;

    private void Update()
    {
        astronauts = GameObject.FindGameObjectsWithTag("Player");

        //System to shows the average level for all astronauts
        if (astronauts.Length == 1)
        {
            sumOfFood = astronauts[0].GetComponent<Stats>().statFood;
            sumOfOxygen = astronauts[0].GetComponent<Stats>().statOxygen;
            sumOfWater = astronauts[0].GetComponent<Stats>().statWater;
        }
        else 
        {
            sumOfFood = 0;
            sumOfOxygen = 0;
            sumOfWater = 0;

            for (int i = 0; i < astronauts.Length; i++)
            {
                sumOfFood += astronauts[i].GetComponent<Stats>().statFood;
                sumOfOxygen += astronauts[i].GetComponent<Stats>().statOxygen;
                sumOfWater += astronauts[i].GetComponent<Stats>().statWater;
            }
        }


        progresBarVSFood.fillAmount = sumOfFood / astronauts.Length;
        progresBarVSOxygen.fillAmount = sumOfOxygen / astronauts.Length;
        progresBarVSWater.fillAmount = sumOfWater / astronauts.Length;

        //System to shows the inventory of selected astronaut
        if (touchedAstronaut != null && panelInventory.activeInHierarchy == false)
        {
            panelInventory.SetActive(true);
            buttonSpawn.SetActive(false);

            cam.enabled = true;

            textName.text = touchedAstronaut.name;
        }

        if (panelInventory.activeInHierarchy == true)
        {
            progresBarFood.fillAmount = stats.statFood;
            progresBarOxygen.fillAmount = stats.statOxygen;
            progresBarWater.fillAmount = stats.statWater;
        }
    }

    public void CloseButton()//Button that close the inventory
    {
        cam.enabled = false;
        panelInventory.SetActive(false);
        buttonSpawn.SetActive(true);
        touchedAstronaut = null;
        cam = null;
        stats = null;
    }

    public void EditableNameButton()//Button that let the user to rename the astronaut's name selected
    {
        textName.interactable = true;
    }

    public void NameIsChanged()//When the user finish to write the name, this apply the writed to the astronaut's name selected
    {
        touchedAstronaut.name = textName.text;
    }

    public void SpawnAstronaut()//Set true the bool if the mouse press the button to spawn it in CameraMovement 
    {
        clickSpawn = true;
        closeButton = true;
    } 
}
