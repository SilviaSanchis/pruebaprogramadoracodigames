using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Atribute/New Atribute")]
public class ScriptableManager : ScriptableObject
{
    //Make the scriptableobject that we need
    public new string name;
    public float recoveryNum;
    public int statType;

    public Sprite artwork;
}
