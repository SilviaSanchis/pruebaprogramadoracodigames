using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDetection : MonoBehaviour
{
    [HideInInspector]
    public bool touchingBot = false;
    [HideInInspector]
    public bool touchingTop = false;
    [HideInInspector]
    public bool touchingLeft = false;
    [HideInInspector]
    public bool touchingRight = false;

    //Top edge detection
    public void TopMouseEntersEdge()
    {
        touchingTop = true;
    }
    public void TopMouseExitEdge()
    {
        touchingTop = false;
    }

    //Bot edge detection
    public void BotMouseEntersEdge()
    {
        touchingBot = true;
    }
    public void BotMouseExitEdge()
    {
        touchingBot = false;
    }

    //Left edge detection
    public void LeftMouseEntersEdge()
    {
        touchingLeft = true;
    }
    public void LeftMouseExitEdge()
    {
        touchingLeft = false;
    }

    //Right edge detection
    public void RightMouseEntersEdge()
    {
        touchingRight = true;
    }
    public void RightMouseExitEdge()
    {
        touchingRight = false;
    }
}
