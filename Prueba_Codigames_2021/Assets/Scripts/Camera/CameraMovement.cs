using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float speedTranslate;
    public GameObject astronautPrefab;
    public GameObject parentForAstro;
    public LayerMask groundCheck;

    [SerializeField]
    private MouseDetection mouseDetection;
    private UiManager uiManager;

    private Vector3 pos;
    private RaycastHit hit;
    private Vector3 pointSpawn;
    void Start()
    {
        mouseDetection = FindObjectOfType<MouseDetection>();
        uiManager = FindObjectOfType<UiManager>();
    }
    void Update()
    {
        pos = transform.position;

        if(mouseDetection.touchingLeft)//Left edge
        {
            pos.z += speedTranslate * Time.deltaTime;
        }
        else if (mouseDetection.touchingRight)//Right edge
        {
            pos.z -= speedTranslate * Time.deltaTime;
        }
        else if (mouseDetection.touchingTop)//Top edge
        {
            pos.x += speedTranslate * Time.deltaTime;
        }
        else if (mouseDetection.touchingBot)//Bot edge
        {
            pos.x -= speedTranslate * Time.deltaTime;
        }
        //Moves the camera
        transform.position = new Vector3(Mathf.Clamp(pos.x, 0f, 7100f), transform.position.y, Mathf.Clamp(pos.z, 1800f, 9000f));
    }

    private void FixedUpdate()
    {
        //Determine the spawn point in reference of the screen to the terrain
        Ray pointScrean = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(pointScrean, out hit, 3000f, groundCheck))
        {
            pointSpawn = hit.point + new Vector3(0, 10f, 0);
        }
        //When button is presed spawn one astronaut
        if (uiManager.clickSpawn == true)
        {
            SpawnAstronaut();
            uiManager.clickSpawn = false;
        }
    }

    void SpawnAstronaut()
    {
        Instantiate(astronautPrefab, pointSpawn, Quaternion.identity, parentForAstro.transform);
    }
}
